Screenshot: 

![Screen Shot 2017-05-16 at 20.42.01.png](https://bitbucket.org/repo/gkkabyA/images/4264468993-Screen%20Shot%202017-05-16%20at%2020.42.01.png)

Laboratory for Artificial Intelligence that had to be solved in 2 ways, using a BreadthFirstSearch algorithm as well as GreedyBestFirstSearch, 


```
6. Problema comisului voiajor – tehnici de rezolvare: BFS, GBFS
Dându-se o hartă a oraşelor din Europa şi a distanţelor între ele 
(precum cea din Figura 6), să se găsească cel mai scurt drum de la Bucureşti
la Paris ştiind că distanţele între oraşe sunt date în Tabel 1
```

![Screen Shot 2017-05-16 at 20.43.45.png](https://bitbucket.org/repo/gkkabyA/images/1609425218-Screen%20Shot%202017-05-16%20at%2020.43.45.png)