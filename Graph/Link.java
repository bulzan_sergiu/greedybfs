package sample.Graph;

/**
 * Created by sergiubulzan on 01/04/2017.
 */
public class Link {
    Node destinationCity;
    Integer distance;

    public Link(Node destinationCity, Integer distance){
        this.destinationCity = destinationCity;
        this.distance = distance;
    }

    public Node getDestinationCity(){
        return destinationCity;
    }

    public int getDistance(){
        return distance;
    }
}
