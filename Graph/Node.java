package sample.Graph;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by sergiubulzan on 01/04/2017.
 */

public class Node{
    List<Link> link;
    String cityName;

    public Node(String cityName){
        this.cityName = cityName;
        this.link = new ArrayList<>();
    }

    public Node(String cityName, List<Link> links){
        this.link = new ArrayList<>();
        this.link = links;
        this.cityName = cityName;
    }

    public void addLink(Link newLink){
        this.link.add(newLink);
    }

    public List<Link> getLinks() {
        return link;
    }

    public String getCityName() {
        return cityName;
    }

    public static Comparator<Node> cmpName = (Node n1, Node n2) -> {
      return n1.cityName.compareTo(n2.cityName);
    };

    public List<Node> getChildrenNodes(){
        List<Node> children = new ArrayList<>();
        for(Link l:getLinks())
            children.add(l.destinationCity);
        return children;
    }

    public String toString(){
        return this.cityName;
    }
}
