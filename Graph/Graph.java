package sample.Graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sergiubulzan on 01/04/2017.
 */
public class Graph {
    List<Node> nodes;
    Map<String,Integer> heuristicValues;

    public Graph(){
        this.nodes = new ArrayList<>();
        this.heuristicValues = new HashMap<>();
    }

    public void setHeuristicValues(Map<String, Integer> heuristicValues) {
        this.heuristicValues = heuristicValues;
    }

    public boolean addHeuristic(String city, int dist){
        if(this.heuristicValues.containsKey(city))
            return false;
        this.heuristicValues.put(city,dist);
        return true;
    }

    public Map<String, Integer> getHeuristicValues() {
        return heuristicValues;
    }

    public Integer getHeuristicValueForCity(String city){
        if(heuristicValues.containsKey(city))
            return heuristicValues.get(city);
        return null;
    }

    public Node getNode(String name){
        for(Node n:nodes){
            if(n.cityName.equals(name))
                return n;
        }
        return null;
    }

    public void addNode(Node n){
        if(getNode(n.cityName) == null)
            nodes.add(n);
        else
            System.out.println("Nodul exista!");
    }

    public void addLink(String cityName, Link link){
        for(Node n:nodes) {
            if (n.cityName == cityName) {
                n.addLink(link);
                return;
            }
        }
    }

    public List<Node> getNodes(){
        return this.nodes;
    }

    public void printLinksForCity(String cityName){
        Node n = getNode(cityName);
        for(Link l : n.getLinks()) {
            System.out.println(l.destinationCity.getCityName() + "  " + l.distance);
        }
    }

    public void linkNodes(String city1, String city2, int distance){
        if(this.getNode(city1) == null) {
            this.addNode(new Node(city1));
        }
        if(this.getNode(city2) == null)
            this.addNode(new Node(city2));
        Link link = new Link(this.getNode(city2),distance);
        this.getNode(city1).addLink(link);
        Link link2 = new Link(this.getNode(city1),distance);
        this.getNode(city2).addLink(link2);
    }
}
