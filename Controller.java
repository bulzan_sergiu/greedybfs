package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sample.Graph.Graph;
import sample.Graph.Node;
import sample.utils.ReadFromFile;
import sample.searchMethods.BFS;
import sample.searchMethods.GBFS;
import sample.utils.Touple;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Controller {
    Graph graph;



    @FXML private Button bfsButton;
    @FXML private Button loadDataFileBtn;
    @FXML private Button gbfsButton;
    @FXML private Button insertBtn;



    @FXML private ComboBox plecareBFSField;
    @FXML private ComboBox sosireBFSField;
    @FXML private ComboBox plecareGBFSField;
    @FXML private TextField oras1Field;
    @FXML private TextField oras2Field;
    @FXML private TextField distanceField;

    @FXML private TextField oras2DistanceToBucharest;
    @FXML private TextField oras1DistanceToBucharest;
    @FXML private Label dist1Lbl;
    @FXML private Label dist2Lbl;




    @FXML private ScrollPane scrollPane;



    public ObservableList<String> getCityNames(){
        List<String> orase = new ArrayList<>();
        for(Node node : graph.getNodes()){
            System.out.println(node);
            orase.add(node.getCityName());
        }
        return FXCollections.observableArrayList(orase);
    }

    public void updateComboBox(){
        //this.plecareBFSField.getItems().clear();
        this.plecareBFSField.setItems(this.getCityNames());
        //this.sosireBFSField.getItems().clear();
        this.sosireBFSField.setItems(this.getCityNames());
        this.plecareGBFSField.setItems(this.getCityNames());
    }

    public void alert(String mesaj){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Atentie");
        alert.setHeaderText(null);
        alert.setContentText(mesaj);

        alert.showAndWait();
    }

    public boolean checkBFSFields(){

        if(this.plecareBFSField.getValue() != null)
            if(this.sosireBFSField.getValue() != null)
                return true;
            else
                alert("Introduceti un oras de sosire!");
        else
            alert("Introduceti un oras de plecare!");
        return false;

    }

    public boolean checkGBFSFields(){

        if(this.plecareGBFSField.getValue() != null)
            return true;
        else
            alert("Introduceti un oras de plecare!");

        return false;

    }

    @FXML
    public void BFSButtonPressed(ActionEvent event){

        if(checkBFSFields()) {
            BFS bfs = new BFS(graph.getNode(plecareBFSField.getValue().toString()), graph.getNode(sosireBFSField.getValue().toString()));
            bfs.compute2();
            populateBFSScrollPane(bfs.getSolution());
        }
    }

    @FXML
    public void setGraphFromFile(){

        this.graph = ReadFromFile.readDistanceGraph();
        this.graph.setHeuristicValues(ReadFromFile.readHeuristicValues());
        this.updateComboBox();
    }


    @FXML
    public void GBFSButtonPressed(ActionEvent event){
        if(checkGBFSFields()) {
            GBFS gbfs = new GBFS(graph.getNode(plecareGBFSField.getValue().toString()), graph.getNode("Bucharest"),graph.getHeuristicValues());
            try{
                gbfs.compute();
                populateGBFSScrollPane(gbfs.getSolution());
            }catch (Exception ex){
                alert("Sorry! Could not get greedy result for this city!");
            }
        }

    }
    public void populateGBFSScrollPane(List<Node> lista){

        File circleF = new File("src/sample/circle.png");
        File arrowF = new File("src/sample/arrow.png");




        VBox aiaMare = new VBox();


        for(Node nod:lista){

            Image circle = new Image(circleF.toURI().toString(),50,50,true,true);
            Image arrow = new Image(arrowF.toURI().toString(),50,50,true,true);

            ImageView circleImg = new ImageView(circle);
            ImageView arrowImg = new ImageView(arrow);

            VBox patrat = new VBox();

            HBox hSus = new HBox();
            hSus.getChildren().add(circleImg);
            hSus.getChildren().add(new Label(nod.getCityName()));

            HBox hJos = new HBox();
            if(lista.indexOf(nod) != lista.size()-1) {
                hJos.getChildren().add(arrowImg);
            }
            Integer dist = graph.getHeuristicValueForCity(nod.getCityName().toString());

            if(dist != null)
                hJos.getChildren().add(new Label(dist.toString()));

            patrat.getChildren().addAll(hSus,hJos);

            aiaMare.getChildren().add(patrat);
        }



        this.scrollPane.setContent(aiaMare);
        this.scrollPane.setPadding(new Insets(20,100,10,100));


    }


    public void populateBFSScrollPane(List<Touple<Node,Integer>> lista){


        File circleF = new File("src/sample/circle.png");
        File arrowF = new File("src/sample/arrow.png");




        VBox aiaMare = new VBox();


        for(Touple<Node,Integer> tuplu:lista){

            Image circle = new Image(circleF.toURI().toString(),50,50,true,true);
            Image arrow = new Image(arrowF.toURI().toString(),50,50,true,true);

            ImageView circleImg = new ImageView(circle);
            ImageView arrowImg = new ImageView(arrow);

            VBox patrat = new VBox();

            HBox hSus = new HBox();
            hSus.getChildren().add(circleImg);
            hSus.getChildren().add(new Label(tuplu.a.getCityName()));

            HBox hJos = new HBox();
            if(lista.indexOf(tuplu) != lista.size()-1) {
                hJos.getChildren().add(arrowImg);
            }
            hJos.getChildren().add(new Label(tuplu.b.toString()));

            patrat.getChildren().addAll(hSus,hJos);

            aiaMare.getChildren().add(patrat);
        }



        this.scrollPane.setContent(aiaMare);
        this.scrollPane.setPadding(new Insets(20,100,10,100));

    }

    @FXML
    public void clearDataPressed(ActionEvent event){
        this.plecareBFSField.setValue("");
        this.plecareGBFSField.setValue("");
        this.sosireBFSField.setValue("");
        oras1Field.setText("");
        oras2Field.setText("");
        oras1DistanceToBucharest.setText("");
        oras2DistanceToBucharest.setText("");
        this.scrollPane.setContent(null);
    }

    public boolean checkInsertFields(){
        if(!oras1Field.getText().isEmpty())
            if(!oras2Field.getText().isEmpty())
                if(!distanceField.getText().isEmpty())
                    return true;
                else
                    alert("Dati o distanta!");
            else
                alert("Introduceti cel de al doilea oras!");
        else
            alert("Introduceti primul oras!");
        return false;
    }

    @FXML
    public void insertBtnPressed(ActionEvent event){
        if(checkInsertFields()){
            int dist;
            try {
                dist = Integer.parseInt(distanceField.getText());
                if(graph == null)
                    graph = new Graph();
                this.graph.linkNodes(oras1Field.getText(),oras2Field.getText(),dist);
                if(!oras1DistanceToBucharest.getText().isEmpty())
                    if(!this.graph.addHeuristic(oras1Field.getText(), Integer.parseInt(oras1DistanceToBucharest.getText())))
                        alert("Orasul 1 are deja valoare!");
                if(!oras2DistanceToBucharest.getText().isEmpty())
                    if(!this.graph.addHeuristic(oras2Field.getText(), Integer.parseInt(oras2DistanceToBucharest.getText())))
                        alert("Orasul 2 are deja valoare!");
                updateComboBox();
            } catch (NumberFormatException e) {
                alert("Distanta trebuie sa fie un numar intreg!");
            }
        }
    }
}
