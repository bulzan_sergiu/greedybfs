package sample.exceptions;

/**
 * Created by sergiubulzan on 01/04/2017.
 */
public class CityNotFoundException extends Exception {
    public CityNotFoundException(){
        super();
    }

    public CityNotFoundException(String msg){
        super(msg);
    }
}
