package sample.searchMethods;

import sample.Graph.Node;

/**
 * Created by sergiubulzan on 01/04/2017.
 */
public class SearchMethod {
    Node startNode;
    Node goalNode;

    public SearchMethod(Node startNode, Node goalNode){
        this.startNode = startNode;
        this.goalNode = goalNode;
    }
}
