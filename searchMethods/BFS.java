package sample.searchMethods;

import sample.Graph.Link;
import sample.Graph.Node;
import sample.utils.Touple;

import java.util.*;

/**
 * Created by sergiubulzan on 01/04/2017.
 */
public class BFS extends UninformedSearchMethod {
    Stack<Grupare> solution;

    public BFS(Node startNode, Node goalNode) {
        super(startNode, goalNode);
        solution = new Stack<>();
    }

    public boolean compute2(){

        if(this.startNode.equals(goalNode)){
            System.out.println("Goal Node Found!");
            System.out.println(startNode);
        }

        Queue<Grupare> queue = new LinkedList<>();

        ArrayList<Node> explored = new ArrayList<>();
        queue.add(new Grupare(this.startNode, 0,null));

        while(!queue.isEmpty()){
            Grupare removed = queue.remove();
            Node current = removed.node;
            int distance = removed.distance;
            Grupare anterior = removed.anterior;

            if(current.getCityName().equals(this.goalNode.getCityName())) {
                if(solution .size() == 0 || solution.peek().distance > distance) {
                    solution.clear();
                    solution.push(removed);
                    while (anterior != null) {
                        solution.push(anterior);
                        anterior = anterior.anterior;
                    }
                }

            }
            else{
                if(current.getLinks().isEmpty())
                    return false;
                else {
                    if(!explored.contains(current)){
                        List<Link> children = current.getLinks();
                        List<Grupare> toAdd = new ArrayList<>();
                        for(Link link:children){
                            Node nod = link.getDestinationCity();
                            int newDistance = distance + link.getDistance();
                            toAdd.add(new Grupare(nod,newDistance,removed));
                        }
                        queue.addAll(toAdd);

                    }
                }
            }
            if(!explored.contains(current))
                explored.add(current);
        }
        return false;
    }


    private class Grupare{
        Node node;
        int distance;
        Grupare anterior;
        public Grupare(Node node, int dist){
            this.node = node;
            this.distance = dist;
        }

        public Grupare(Node node, int dist,Grupare anterior){
            this.node = node;
            this.distance = dist;
            this.anterior =anterior;
        }
    }


    public List<Touple<Node,Integer>> getSolution(){
        List<Touple<Node,Integer>> lista= new ArrayList<>();
        if(solution.size() > 0){
            while (solution.size()>0) {
                Grupare grup = solution.pop();
                lista.add(new Touple<>(grup.node,grup.distance));
            }
        }
        return lista;
    }
}
