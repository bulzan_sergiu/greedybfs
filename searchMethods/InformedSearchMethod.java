package sample.searchMethods;

import sample.Graph.Node;

/**
 * Created by sergiubulzan on 01/04/2017.
 */

public  class InformedSearchMethod extends SearchMethod{
    public InformedSearchMethod(Node startNode, Node goalNode) {
        super(startNode, goalNode);
    }
}
