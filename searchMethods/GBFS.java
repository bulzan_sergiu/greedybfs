package sample.searchMethods;

import sample.Graph.Node;

import java.util.*;

/**
 * Created by sergiubulzan on 01/04/2017.
 */

public class GBFS extends InformedSearchMethod {


    private List<Node> solution;
    private Map<String,Integer> heuristicValues;



    public GBFS(Node startNode, Node goalNode,Map<String,Integer> heuristicValues) {
        super(startNode, goalNode);
        this.heuristicValues = heuristicValues;
    }



    public boolean compute(){

        if(this.startNode.equals(goalNode)){
            System.out.println("Am gasit la primul pas!");
            System.out.println(startNode);
        }

        Queue<Node> queue = new LinkedList<>();

        ArrayList<Node> explored = new ArrayList<>();
        queue.add(this.startNode);


        while(!queue.isEmpty()){
            Node current = queue.remove();

            if(current.getCityName().equals(this.goalNode.getCityName())) {
                explored.add(current);
                solution = new ArrayList<>(explored);
                return true;
            }


            else{
                if(current.getLinks().isEmpty())
                    return false;
                else {
                    if(!explored.contains(current)) {
                        List<Node> children = current.getChildrenNodes();
                        Node minim = getMinimalValue(children);
                        queue.add(minim);
                    }

                }
            }
            if(!explored.contains(current))
                explored.add(current);
        }

        return false;
    }

    public Node getMinimalValue(List<Node> nodes){
        Node result = nodes.get(0);
        int minDistance = -1;

        minDistance = heuristicValues.get(nodes.get(0).getCityName());

        for(Node n:nodes){
            int crtHeuristicValue = -1;
            crtHeuristicValue = heuristicValues.get(n.getCityName());
            if(crtHeuristicValue != -1 && minDistance != -1 && crtHeuristicValue < minDistance){
                minDistance =crtHeuristicValue;
                result = n;
            }
        }

        if(minDistance == -1)
            System.out.println("err!!!");


        return result;
    }

    public List<Node> getSolution(){
        return this.solution;
    }
}
