package sample.utils;

/**
 * Created by sergiubulzan on 01/04/2017.
 */
public class Touple<A,B> {
    public A a;
    public B b;
    public Touple(A a, B b){
        this.a = a;
        this.b = b;
    }
}
