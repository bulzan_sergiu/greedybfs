package sample.utils;

import sample.Graph.Graph;
import sample.exceptions.CityNotFoundException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sergiubulzan on 01/04/2017.
 */
public class ReadFromFile {
    public static Graph readDistanceGraph(){
        Graph graph = new Graph();
        try (BufferedReader br = new BufferedReader(new FileReader("src/sample/distances.txt"))) {

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                String[] splitted = sCurrentLine.split(" ");
                if(splitted.length ==3){
                    graph.linkNodes(splitted[0],splitted[1],Integer.parseInt(splitted[2]));

                }
                else{
                    System.out.println("Eroare la citirea liniei!");
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return graph;
    }

    public static Integer getHeuristicFunction(String cityName) throws CityNotFoundException {
        try (BufferedReader br = new BufferedReader(new FileReader("src/sample/bucharestDistances.txt"))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                String[] splitted = sCurrentLine.split(" ");
                if(splitted[0].equals(cityName))
                    return Integer.parseInt(splitted[1]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new CityNotFoundException();
    }

    public static Map<String,Integer> readHeuristicValues(){
        Map<String,Integer> values = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader("src/sample/bucharestDistances.txt"))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                String[] splitted = sCurrentLine.split(" ");
                values.put(splitted[0],Integer.parseInt(splitted[1]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return values;
    }
}
